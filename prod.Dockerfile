FROM golang:alpine as builder

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /go/src/prayeralarm-go
ADD ./ ./
# RUN go install gitlab.com/dev.786zshan/prayeralarm-go
RUN GOARCH=386 GOOS=linux go build -ldflags="-w -s" -o /go/bin/prayeralarm-go

FROM scratch
COPY --from=builder /go/bin/prayeralarm-go /go/bin/prayeralarm-go
EXPOSE 8080
ENTRYPOINT ["/go/bin/prayeralarm-go"]
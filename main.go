package main

import (
	"log"
	"time"
	"gitlab.com/dev.786zshan/prayeralarm-go/internal/mp3"
	"gitlab.com/dev.786zshan/prayeralarm-go/internal/models"
)

func runMonthlyTask(year int, month time.Month, day int, mp3Channel chan string) error {
	adhanSlice, err := models.GetAdhanCommands(year, month)
	if err!=nil {
		return err
	}
	models.PrintAdhanCommands(adhanSlice, year, month)

	for _, adhanCommand := range adhanSlice {
		timeTillNextAdhan := adhanCommand.Time.Sub(time.Now())
		log.Printf("Waiting %s for %s adhan...", timeTillNextAdhan, adhanCommand.Type)
		time.Sleep(timeTillNextAdhan)
		
		log.Printf("Running %s adhan at %s...", adhanCommand.Type, adhanCommand.Time)
		mp3Channel <- adhanCommand.AdhanFile
	}

	dateNextMonth := time.Now().AddDate(0, 1, 0)
	year_n, month_n, day_n := dateNextMonth.Date()
	monthlyErr := runMonthlyTask(year_n, month_n, day_n, mp3Channel)
	if monthlyErr!=nil {
		return err
	}

	return nil
}

func main() {
	mp3Channel := make(chan string)
	go mp3.InitializeMP3Player(mp3Channel)

	year, month, day := time.Now().Date()
	err := runMonthlyTask(year, month, day, mp3Channel)
	if err!=nil {
		log.Panic(err.Error())
	}
}
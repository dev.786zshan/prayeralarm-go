# Prayer Alarm
A cross-platform islamic prayer alarm (Golang based).  
The prayer alarm binary will run the adhan prayer call (audio) based on prayer timings retrieved from the following [Adhan API](https://aladhan.com/prayer-times-api) for a specified location.  
By default, the prayer call timings are set for:
- city = auckland  
- country = new zealand  
  
Prayer timings are retrieved on a monthly basis (starting with current month) with the next prayer call being being the actual next prayer timing as of current local time.  

## Prayer Call offsetting
Offsetting prayer call times is also supported. Prayer call's can be offset by a specified number of minutes by providing an optional **offsets** flag when running the binary.  
For example, to respectively offset the _Maghrib_ and _Isha_ prayer calls to run 5 mins later and 3 mins earlier, the binary can be run with the following flag: **-offsets "0 0 0 5 -3"**  
By default, offsets for all prayer times are set to **0**.


# Pre-requisites
- Golang (developed on v1.13)
- Pre-requsites for sound player dependency [Oto](https://github.com/hajimehoshi/oto) - based on OS

# Build binary
```
go build
```
## Install to go path
```
go install
```

# Running binary
## Run in foreground
```
./prayeralarm-go
```
## Run with optional city, country and offset flags
```
./prayeralarm-go -city auckland -country "new zealand" -offsets "5 0 -5 -10 0"
```
## Run in background with log file
```
nohup ./prayeralarm-go > adhan.log &
```
### Kll background process
```
kill $(ps -ef | grep prayeralarm-go| cut -f4 -d" " | head -1)
```

# Dependencies
- [Oto](https://github.com/hajimehoshi/oto) for cross-platform MP3 playback (playing Adhan audio)
- [packr2](https://github.com/gobuffalo/packr/tree/master/v2) for converting adhan audio files (mp3) into bytes which have been committed to source control as a single [file](./internal/mp3/mp3-files.go); this allows us to distribute the generated binary without depending on external adhan audio files on the file system

# Licence
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

---

# TODO
- Testing
- Gitlab releases
  - tagging
    - binary release
      - raspberry Pi
- dockerize to run on raspberry pi
- implement CICD pipelines
  - automate CICD to deploy onto raspberry PI
- ToDateTime method shouldnt use local time; instead should use api based timezone

---
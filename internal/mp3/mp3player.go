package mp3

import (
	"io"
	"log"
	"bytes"

	"gitlab.com/dev.786zshan/prayeralarm-go/internal/consts"
	"github.com/gobuffalo/packr/v2"
	"github.com/hajimehoshi/oto"
	"github.com/hajimehoshi/go-mp3"
)

func InitializeMP3Player(filechan chan string) {
	box := packr.New(consts.PackrBox, consts.MP3Files)

	player, err := oto.NewPlayer(44100, 2, 2, 44100)	// oto.NewPlayer(d.SampleRate(), 2, 2, 8192)
	if err != nil {
		log.Fatal(err)
	}
	defer player.Close()

	for {
		select {
			case filename := <-filechan:
				fileData, err := box.Find(filename)
				if err != nil {
					log.Panic(err.Error())
				}
				file := bytes.NewReader(fileData)

				// // Used to read file from local file system
				// file, err := os.Open("../../assets/" + filename)
				// if err != nil {
				// 	log.Panic(err.Error())
				// }
				// defer file.Close()

				decoder, err := mp3.NewDecoder(file)
				if err != nil {
					log.Panic(err.Error())
				}

				if _, err := io.Copy(player, decoder); err != nil {
					log.Panic(err.Error())
				}
		}
	}
}
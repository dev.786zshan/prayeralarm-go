package models

type AdhanError struct {
	s string
}

func NewAdhanError(text string) error {
	return &AdhanError{text}
}

func (e *AdhanError) Error() string {
		return e.s
}
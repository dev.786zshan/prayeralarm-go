package models

import (
	"time"
	"reflect"
	"fmt"
	"gitlab.com/dev.786zshan/prayeralarm-go/internal/utils"
	"flag"
	"strings"
)

type AdhanCommand struct {
	Time time.Time
	Type string
	AdhanFile string
}

func getOffsets() (map[string]int, error) {
	offsetPtr := flag.String("offsets", "0 0 0 0 0", "adhan offsets (in mins) for the 5 daily adhans (fajr, dhuhr, asr, maghrib, isha)")
	flag.Parse()
	offsetsStr := strings.Split(*offsetPtr, " ")
	if len(offsetsStr) != 5 {
		return nil, NewAdhanError("'offsets' flag must be a string of 5 integers - e.g. \"5 0 10 5 12\" ")
	}
	offsetsInt, err := utils.SliceAtoi(offsetsStr)
	if err!=nil {
		return nil, err
	}
	adhanOffsetMap := map[string]int{
    "Fajr": offsetsInt[0],
    "Dhuhr":   offsetsInt[1],
    "Asr": offsetsInt[2],
    "Maghrib": offsetsInt[3],
    "Isha": offsetsInt[4],
	}
	return adhanOffsetMap, nil
}

func GetAdhanCommands(year int, month time.Month) ([]AdhanCommand, error) {
	adhanOffsetMap, err := getOffsets()
	if err!=nil {
		return nil, err
	}

	monthlyAdhanCalender, err := GetMonthlyPrayerTimes(month, year)
	if err!=nil {
		return nil, err
	}
	adhanSlice := []AdhanCommand{}

	for i, elem := range monthlyAdhanCalender.Data {
		val := reflect.ValueOf(elem.Timings)
		typeOfS := val.Type()
		for j := 0; j < val.NumField(); j++ {
			adhanFile := "Adhan-Turkish.mp3"
			if typeOfS.Field(j).Name == "Fajr" {
				adhanFile = "Adhan-Fajr.mp3"
			}
			hoursMinutes := fmt.Sprintf("%s",val.Field(j).Interface())
			adhanTime, err := utils.ToDateTime(hoursMinutes, year, month, i+1)
			if err!=nil {
				return nil, err
			}

			adhanCommand := &AdhanCommand{
				Time: adhanTime,
				Type: typeOfS.Field(j).Name,
				AdhanFile: adhanFile,
			}

			// Adding custom adhan timing offsets (if passed in as flag)
			adhanCommand.Time = adhanCommand.Time.Add(time.Minute * time.Duration(adhanOffsetMap[adhanCommand.Type]))

			if adhanCommand.Time.After(time.Now()) {
				adhanSlice = append(adhanSlice, *adhanCommand)
			}
		}
	}
	return adhanSlice, nil
}

func PrintAdhanCommands(adhanSlice []AdhanCommand, year int, month time.Month) {
	gap := ""
	if len(month.String())<4 {
		gap = "\t"
	}
	fmt.Println("┌────────────────┐")
	fmt.Printf("│ %s, %d%s │\n", month, year, gap)
	fmt.Println("├────────────────┼───────────────────────┬────────────────────────────────┐")
	fmt.Println("│     Adhan      │         Audio         │          Prayer Time           │")
	fmt.Println("├────────────────┼───────────────────────┼────────────────────────────────┤")
	for _, adhanCommand := range adhanSlice {
		adhanCommand.Print()
	}
	fmt.Println("└────────────────┴───────────────────────┴────────────────────────────────┘")
}

func (ac *AdhanCommand) Print() {
	gap := ""
	if len(ac.Type)<7 {
		gap = "\t"
	}
	fmt.Printf("│ %s%s\t │ %s\t │ %s │\n", ac.Type, gap, ac.AdhanFile, ac.Time)
	if ac.Type == "Isha" {
		fmt.Print("├────────────────┼───────────────────────┼────────────────────────────────┤\n")
	}
}
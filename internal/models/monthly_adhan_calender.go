package models

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"log"
	"strconv"
	"time"
	"flag"
)

type MonthlyAdhanCalender struct {
  Data []struct {
		Timings struct {
			Fajr string
			Dhuhr string
			Asr string
			Maghrib string
			Isha string
		}
	}
}

func GetMonthlyPrayerTimes(month time.Month, year int) (*MonthlyAdhanCalender, error) {
	cityPtr := flag.String("city", "auckland", "city for which adhan timings are to be retrieved")
	countryPtr := flag.String("country", "new zealand", "country for which adhan timings are to be retrieved")
	flag.Parse()
	log.Printf("Retrieving adhan timings for %s %d in %s, %s", month, year, *cityPtr, *countryPtr)

	req, err := http.NewRequest("GET", "http://api.aladhan.com/v1/calendarByCity", nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("city", *cityPtr)
	q.Add("country", *countryPtr)
	q.Add("month", strconv.Itoa(int(month)))
	q.Add("year", strconv.Itoa(year))
	q.Add("method", "3")
	req.URL.RawQuery = q.Encode()

	resp, err := http.Get(req.URL.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var monthlyAdhanCalender = &MonthlyAdhanCalender{}
	json.Unmarshal(body, &monthlyAdhanCalender)

	return monthlyAdhanCalender, nil
}
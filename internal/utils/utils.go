package utils

import (
	"time"
	"strings"
	"strconv"
)

func ToDateTime(timeString string, year int, month time.Month, day int) (time.Time, error) {
	hoursMinutes := strings.Split(timeString, " ")[0]		// HH:mm
	hour, err := strconv.Atoi(hoursMinutes[:2])
	if err!=nil {
		return time.Time{}, err
	}
	min, err := strconv.Atoi(hoursMinutes[3:])
	if err!=nil {
		return time.Time{}, err
	}
	return time.Date(year, month, day, hour, min, 0, 0, time.Local), nil
}

func SliceAtoi(sa []string) ([]int, error) {
	si := make([]int, 0, len(sa))
	for _, a := range sa {
			i, err := strconv.Atoi(a)
			if err != nil {
					return si, err
			}
			si = append(si, i)
	}
	return si, nil
}
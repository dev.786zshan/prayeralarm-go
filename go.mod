module gitlab.com/dev.786zshan/prayeralarm-go

go 1.13

require (
	github.com/gobuffalo/packr/v2 v2.7.1
	github.com/hajimehoshi/go-mp3 v0.2.1
	github.com/hajimehoshi/oto v0.5.4
)

FROM golang:buster as builder

RUN apt update && apt install -y libasound2-dev
RUN go get github.com/githubnemo/CompileDaemon

COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

# ENV GOOS=darwin GOARCH=amd64
WORKDIR /go/src/prayeralarm-go
ADD ./ ./

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD CompileDaemon -log-prefix=false -build="go build" -command="./prayeralarm-go"